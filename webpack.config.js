const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");

const IS_PRODUCTION = process.env.NODE_ENV === "production";

const DIST_DIR = path.resolve(__dirname, "dist");
const SRC_DIR = path.resolve(__dirname, "src");
const MANIFEST_FILE = "manifest.json";

const manifestPath = path.join(SRC_DIR, MANIFEST_FILE);

module.exports = {
  mode: IS_PRODUCTION ? "production" : "development",
  output: {
    filename: "[name]/index.js",
    path: DIST_DIR,
    clean: true,
  },
  entry: {
    background: "./src/background/index.js",
    content: "./src/content/index.js",
    popup: "./src/popup/index.js",
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader",
            options: { presets: ["@babel/preset-env"], plugins: ["@babel/plugin-transform-runtime"] },
          },
        ],
      },
    ],
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        { from: manifestPath, to: DIST_DIR },
        { from: "./src/popup/index.html", to: DIST_DIR + "/popup/" },
        { from: "./src/icons/", to: DIST_DIR + "/icons/" },
      ],
    }),
  ],
  devtool: IS_PRODUCTION ? "" : "inline-source-map",
};
