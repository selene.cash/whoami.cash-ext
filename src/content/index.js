// whoami.cash browser extension
// content-script.js : gets injected into every page
// listens for CashID messages

(function () {
  window.addEventListener("message", function (event) {
    if (event.source == window && event.data && event.data.source == "cashid") {
      // TODO: check message origin to make sure it's coming from active tab
      browser.runtime.sendMessage({ ...event.data, origin: event.origin });
    }
  });
})();
