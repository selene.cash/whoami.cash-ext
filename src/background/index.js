// background-script.js
// runs client-side persistent logic

function cashIdUriToObject(uri) {
  const _cashid = [
    ...uri.matchAll(
      /(?<intent>cashid:)(?:[\/]{2})?(?<domain>[^\/]+)(?<path>\/[^\?]+)(?<parameters>\?.+)/g
    ),
  ][0];

  _cashid.groups.parameters = [
    ..._cashid.groups.parameters.matchAll(
      /((?:[\?\&]a=)(?<action>[^\&]+))?((?:[\?\&]d=)(?<data>[^\&]+))?((?:[\?\&]r=)(?<required>[^\&]+))?((?:[\?\&]o=)(?<optional>[^\&]+))?((?:[\?\&]x=)(?<nonce>[^\&]+))?/g
    ),
  ][0].groups;

  return { ..._cashid.groups, requestUri: _cashid[0] };
}

(function () {
  browser.runtime.onMessage.addListener(handleMessages);

  // valid actions/functions
  const messageDict = {
    heartbeat: handleHeartbeat,
  };

  // message to function handler
  function handleMessages(message) {
    console.log("[whoami] background script got message", message);
    if (Object.keys(messageDict).indexOf(message.type) > -1) {
      console.log(`executing ${message.type}`);
      messageDict[message.type](message);
    }
  }

  // handles signal from websites that support CashID
  function handleHeartbeat(message) {
    // TODO: ensure tab matches heartbeat
    browser.tabs.query({ active: true, currentWindow: true }).then(
      function (tabs) {
        const tabId = tabs[0].id;
        browser.pageAction.show(tabId);

        // TODO: cashid state per tab/domain
        const _cashid = cashIdUriToObject(message.uri);

        console.log(_cashid);

        return browser.cookies
          .set({
            name: "PHPSESSID",
            value: _cashid.parameters["data"],
            url: message.origin,
            path: "/",
            httpOnly: true,
            sameSite: "lax",
          })
          .then(() =>
            browser.storage.local.set({
              _cashid: _cashid,
            })
          );
      },
      function (err) {
        console.error(`[whoami] ${err}`);
      }
    );
  }
})();
