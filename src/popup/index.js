import {
  instantiateSecp256k1,
  generatePrivateKey,
  instantiateRipemd160,
  instantiateSha256,
  encodeCashAddress,
  decodeCashAddress,
  binToHex,
  hexToBin,
  utf8ToBin,
  binToBase64,
} from "@bitauth/libauth";

function fetchJson(url, payload, method = "GET") {
  console.log(payload);
  return fetch(url, {
    method: method,
    headers: { "Content-Type": "application/json" },
    body: payload,
  }).then(function (response) {
    return response.json();
  });
}

(function () {
  // ----------------------------
  // State

  let _cashid = {};
  let _identities = [];

  // ----------------------------
  // Global Event Listeners

  setupEventListeners();

  function setupEventListeners() {
    // when browser popup loads
    window.addEventListener("load", function onPageActionLoad() {
      // get current data from browser storage
      browser.storage.local.get(null).then(handleLoadData);
    });

    // when any button on popup is clicked
    window.addEventListener("click", function (event) {
      switch (event.target.id) {
        case "newIdentityButton":
          handleNewIdentityButton();
          break;
        default:
          handleClick(event);
          break;
      }
    });
  }

  // ----------------------------
  // Individual Event handlers

  function handleLoadData(data) {
    console.log("[whoami] popup loaded data");
    console.log(data);

    _cashid = { ...data._cashid };
    _identities = [...data._identities];
    render();
  }

  function handleClick(event) {
    console.log(`got click ${event.target.value}`, event.target);
    const identity = _identities.find((i) => i.id == event.target.value);
    console.log("select identity");
    console.log(identity);

    if (identity) {
      handleSelectIdentityButton(identity);
    }
  }

  async function handleNewIdentityButton() {
    const name = prompt("What would you like to name this identity?");

    const identity = await createNewIdentity(name);
    console.log("got identity");
    console.log(identity);
    _identities.push({ ...identity, id: _identities.length + 1 });
    browser.storage.local.set({ _identities });

    render();
  }

  async function handleSelectIdentityButton(identity) {
    const secp256k1 = await instantiateSecp256k1();
    const ripemd160 = await instantiateRipemd160();
    const sha256 = await instantiateSha256();

    const privKey = Uint8Array.from(identity.privKey.split(","));

    const pubKey = secp256k1.derivePublicKeyCompressed(privKey);
    const pubKeyValid = secp256k1.validatePublicKey(pubKey);
    console.log("pubKeyValid", pubKeyValid);

    const hash = ripemd160.hash(sha256.hash(pubKey));
    const address = encodeCashAddress("bitcoincash", "P2PKH", hash);

    const magic = `\x18Bitcoin Signed Message:\n`;
    const payload = _cashid.requestUri;
    const length = payload.length.toString(16);
    const preimage = new Uint8Array([
      ...utf8ToBin(magic),
      ...hexToBin(length),
      ...utf8ToBin(payload),
    ]);
    const message = sha256.hash(sha256.hash(preimage));

    const recoverable = secp256k1.signMessageHashRecoverableCompact(
      privKey,
      message
    );

    const signature = new Uint8Array([
      ...[31 + recoverable.recoveryId],
      ...recoverable.signature,
    ]);

    console.log(address);
    console.log(binToBase64(signature));
    console.log(payload);

    sendCashIdResponse(_cashid, address, binToBase64(signature));
  }

  // ----------------------------
  // Identity Management

  async function createNewIdentity(name) {
    const secp256k1 = await instantiateSecp256k1();

    const privKey = generatePrivateKey(() =>
      window.crypto.getRandomValues(new Uint8Array(32))
    );

    console.log("creating identity");
    console.log(privKey);

    const identity = secp256k1.validatePrivateKey(privKey)
      ? {
          name,
          privKey: privKey.toString(),
        }
      : null;

    console.log(identity);
    return identity;
  }

  // ----------------------------
  // UI Rendering

  function render() {
    document.getElementById("cashid_domain").innerText = _cashid["domain"];
    renderIdentityList();
  }

  function renderIdentityList() {
    console.log(_identities);
    document.getElementById("identity_list").innerHTML = _identities
      .map(
        (identity) =>
          `<li><button type="button" value="${identity.id}">${identity.name}</button></li>`
      )
      .join("");
  }

  // ----------------------------

  function sendCashIdResponse(challenge, address, signature) {
    console.log("cashid challenge");
    console.log(challenge);
    const payload = JSON.stringify({
      request: challenge["requestUri"],
      address: address,
      signature: signature,
      metadata: {},
    });

    const url = "https://" + challenge["domain"] + challenge["path"];

    fetchJson(url, payload, "POST")
      .then(function (data) {
        console.log("cashid repsonse");
        console.log(data);
      })
      .catch(function (err) {
        console.log("error");
        console.error(err);
      });
  }
})();
